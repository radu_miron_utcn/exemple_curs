package edu.utcn.str.lecture3.ex5deadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        Object l1 = new Object();
        Object l2 = new Object();
        new MyThread(l1, l2).start();
        new MyThread(l2, l1).start();
    }
}
