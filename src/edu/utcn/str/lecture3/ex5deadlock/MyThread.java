package edu.utcn.str.lecture3.ex5deadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private Object l1;
    private Object l2;

    public MyThread(Object l1, Object l2) {
        this.l1 = l1;
        this.l2 = l2;
    }

    @Override
    public void run() {
        new Activity(1).execute();

        synchronized (l1) {
            new Activity(2).execute();

            synchronized (l2) {
                new Activity(3).execute();
            }
        }

        new Activity(4).execute();
    }

    class Activity {
        private int id;

        public Activity(int id) {
            this.id = id;
        }

        public void execute() {
            System.out.println(
                    String.format("%s executes activity %d", Thread.currentThread(), id));
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException ignored) {
//            }
        }
    }
}
