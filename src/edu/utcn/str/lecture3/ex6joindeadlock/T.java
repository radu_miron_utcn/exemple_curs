package edu.utcn.str.lecture3.ex6joindeadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class T extends Thread {
    private Thread t;
    public T(Thread t) {
        this.t = t;
    }
    public void run() {
        try {
            t.join();
        } catch (InterruptedException e) {
        }
        System.out.println("I'm also done");
    }

    public void setT(T t) {
        this.t = t;
    }
}
