package edu.utcn.str.lecture3.app1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();

        new MyThread(counter).start();
        new MyThread(counter).start();
        new MyThread(counter).start();

        Thread.sleep(1000);

        System.out.println(counter.getValue());
    }
}
