package edu.utcn.str.lecture3.app1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Counter {
    private volatile int value;

    public synchronized void increment() {
        value += 1;
    }

    public int getValue() {
        return value;
    }
}
