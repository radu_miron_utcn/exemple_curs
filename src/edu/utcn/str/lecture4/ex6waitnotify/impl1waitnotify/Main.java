package edu.utcn.str.lecture4.ex6waitnotify.impl1waitnotify;

import java.io.IOException;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    static final Object monitor = new Object();
    public static void main(String[] args) {
        new NotifierThread().start();
        new WaiterThread().start();
    }
}
