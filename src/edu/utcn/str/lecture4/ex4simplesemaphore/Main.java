package edu.utcn.str.lecture4.ex4simplesemaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(1, true);
        new ThreadSemaphore(semaphore).start();
        new ThreadSemaphore(semaphore).start();
        new ThreadSemaphore(semaphore).start();
    }
}
