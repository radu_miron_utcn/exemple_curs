package edu.utcn.str.lecture4.ex2concurrentmodifexc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        List<String> list = new CopyOnWriteArrayList<>();
        list.add("a");
        list.add("c");
        list.add("des");
        list.add("e");
        list.add("des");

        for (String s : list) {
            if (s.equals("des")) {
                list.remove(s);
            }
        }

//        List<String> list = new ArrayList<>();
//        list.add("a");
//        list.add("c");
//        list.add("des");
//        list.add("e");
//        list.add("des");
//
//        for (String s : list) {
//            if (s.equals("des")) {
//                list.remove(s);
//            }
//        }

//        Iterator<String> it = list.iterator();
//        while (it.hasNext()) {
//            String s = it.next();
//
//            if (s.equals("des")) {
//                it.remove();
//            }
//
//        }

        System.out.println(list.size());
    }
}
