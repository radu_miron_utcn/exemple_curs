package edu.utcn.str.lecture2.ex2runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread implements Runnable {
    private String name;

    public MyThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(String.format("Thread %s - message %d", Thread.currentThread().getName(), i));

            try {
                Thread.sleep(50);
            } catch (InterruptedException ignore) {
            }
        }
    }

    public void start() {
        new Thread(this).start();
    }
}
