package edu.utcn.str.lecture2.ex2runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        MyThread t1 = new MyThread("Thread-1");
        MyThread t2 = new MyThread("Thread-2");

//        new Thread(t1).start();
//        new Thread(t2).start();

        t1.start();
        t2.start();

        // it is executed on the main thread
        t1.run();
    }
}
