package edu.utcn.str.lecture2.ex1thread;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    public MyThread(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(String.format("Thread %s - message %d", Thread.currentThread().getName(), i));

            try {
                Thread.sleep(50);
            } catch (InterruptedException ignore) {
            }
        }
    }
}
