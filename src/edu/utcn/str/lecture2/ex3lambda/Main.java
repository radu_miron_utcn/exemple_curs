package edu.utcn.str.lecture2.ex3lambda;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {

        Runnable runnable1 = () -> printMessages();

        Runnable runnable2 = () -> printMessages();

        new Thread(runnable2, "Thread-1").start();
        new Thread(runnable2, "Thread-2").start();
        printMessages();
    }

    private static void printMessages() {
        for (int i = 0; i < 20; i++) {
            System.out.println(String.format("Thread %s - message %d", Thread.currentThread().getName(), i));

            try {
                Thread.sleep(50);
            } catch (InterruptedException ignore) {
            }
        }
    }
}
