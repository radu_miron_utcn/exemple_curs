package edu.utcn.str.lecture2.ex4interrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Sleeper sleeper = new Sleeper();
        Interrupter interrupter = new Interrupter(sleeper);
        sleeper.start();
        interrupter.start();
    }
}
