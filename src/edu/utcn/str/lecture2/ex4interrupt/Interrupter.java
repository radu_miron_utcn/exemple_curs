package edu.utcn.str.lecture2.ex4interrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class Interrupter extends Thread {
    private Sleeper sleeper;

    public Interrupter(Sleeper sleeper) {
        this.sleeper = sleeper;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ignore) {
        }

        System.out.println("I'll interrupt the Sleeper");

        sleeper.interrupt();
    }
}
