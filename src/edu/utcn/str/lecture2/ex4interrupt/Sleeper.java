package edu.utcn.str.lecture2.ex4interrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class Sleeper extends Thread {
    @Override
    public void run() {
        try {
            System.out.println("Read BNR exchange rate!");
            Thread.sleep(24 * 60 * 60 * 1000);
        } catch (InterruptedException e) {
            System.out.println("I've been interrupted!");
        }
    }
}
