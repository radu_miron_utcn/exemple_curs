package edu.utcn.str.lecture1.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
public class BMW extends Car {
    public BMW(String color, int numOfWheels) {
        super(color, numOfWheels);
    }

    @Override
    public void move() {
        super.move();
        System.out.println("And it's a BMW!");
    }

    public void fly() {

    }
}
