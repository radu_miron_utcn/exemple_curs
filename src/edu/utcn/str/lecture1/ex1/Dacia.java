package edu.utcn.str.lecture1.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Dacia extends Car {
    public Dacia(String color, int numOfWheels) {
        super(color, numOfWheels);
    }

    @Override
    public void move(){
        System.out.println("Dacia moves fast!");
    }
}
