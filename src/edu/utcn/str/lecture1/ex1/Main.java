package edu.utcn.str.lecture1.ex1;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Chose: 1.Dacia; 2.BMW");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        Car car;

        if (choice == 1) {
            car = new Dacia("blue", 4);
        } else if (choice == 2) {
            car = new BMW("black", 4);
        } else {
            throw new IllegalArgumentException("Invalid choice");
        }

        car.start();
        car.move(); // here
        car.stop();


        // type cast
        BMW bmw = new BMW("white", 4);
        bmw.stop();
        bmw.start();
        bmw.move();
        bmw.fly();

        Car carBmw = (BMW) bmw;
//        carBmw.fly();

        Car car1 = new Car("blue", 4);
        BMW bmw1 = (BMW) car1;
        bmw1.fly();

    }
}
