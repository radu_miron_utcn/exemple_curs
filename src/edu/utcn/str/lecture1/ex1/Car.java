package edu.utcn.str.lecture1.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    public static final int NUM_OF_ENGINES = 1;
    private String color;
    private int numOfWheels;

    public Car(String color, int numOfWheels) {
        this.color = color;
        this.numOfWheels = numOfWheels;
    }

    public static void printNumOfEngines() {
        System.out.println(NUM_OF_ENGINES);
    }

    public void start() {
        System.out.println("The car start.");
    }

    public void move() {
        System.out.println("The car moves.");
    }

    public void stop() {
        System.out.println("The car start.");
    }
}
