package edu.utcn.str.lecture6.app1;

import edu.utcn.str.lecture6.utils.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class Thread2 extends Thread {
    private Object lock1;
    private Object lock2;
    private int actMin;
    private int actMax;
    private int tranTemp;

    public Thread2(Object lock1, Object lock2, int actMin, int actMax, int tranTemp) {
        this.lock1 = lock1;
        this.lock2 = lock2;
        this.actMin = actMin;
        this.actMax = actMax;
        this.tranTemp = tranTemp;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity(1);

        synchronized (lock1) {
            synchronized (lock2) {
                ActivityUtils.doTimedActivity(actMin, actMax, 2);

                try {
                    Thread.sleep(tranTemp * 10);
                } catch (InterruptedException e) {
                }
            }
        }

        ActivityUtils.doActivity(3);
    }
}
