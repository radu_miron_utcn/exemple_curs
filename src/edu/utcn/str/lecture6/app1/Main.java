package edu.utcn.str.lecture6.app1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object lock1 = new Object();
        Object lock2 = new Object();

        new Thread1(lock1, 2,4,4).start();
        new Thread1(lock2, 2,5,5).start();
        new Thread2(lock1, lock2, 2,5,5).start();
    }
}
