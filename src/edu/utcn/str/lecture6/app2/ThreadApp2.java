package edu.utcn.str.lecture6.app2;

import edu.utcn.str.lecture6.utils.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadApp2 extends Thread {
    private Object lock1;
    private Object lock2;
    private int act1Min;
    private int act1Max;
    private int act2Min;
    private int act2Max;
    private int tranTemp;

    public ThreadApp2(Object lock1, Object lock2, int act1Min, int act1Max, int act2Min, int act2Max, int tranTemp) {
        this.lock1 = lock1;
        this.lock2 = lock2;
        this.act1Min = act1Min;
        this.act1Max = act1Max;
        this.act2Min = act2Min;
        this.act2Max = act2Max;
        this.tranTemp = tranTemp;
    }

    @Override
    public void run() {
        ActivityUtils.doTimedActivity(act1Min, act1Max, 1);
        synchronized (lock1) {
            ActivityUtils.doTimedActivity(act2Min, act2Max, 2);
            synchronized (lock2) {
                ActivityUtils.doActivity(3);

                // temp. transition
                try {
                    Thread.sleep(tranTemp * 10);
                } catch (InterruptedException e) {
                }
            }
        }
        ActivityUtils.doActivity(4);
    }
}
