package edu.utcn.str.lecture6.app2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object lock1 = new Object();
        Object lock2 = new Object();
        new ThreadApp2(lock1, lock2, 2, 4, 4, 6, 4).start();
        new ThreadApp2(lock2, lock1, 3, 5, 5, 7, 5).start();
    }
}
