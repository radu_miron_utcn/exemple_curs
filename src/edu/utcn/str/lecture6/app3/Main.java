package edu.utcn.str.lecture6.app3;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object lock1 = new Object();

        new ThreadApp3(lock1, 3, 6, 5).start();
        new ThreadApp3(lock1, 4, 7, 3).start();
        new ThreadApp3(lock1, 5, 7, 6).start();
    }
}
