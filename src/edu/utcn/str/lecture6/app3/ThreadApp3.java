package edu.utcn.str.lecture6.app3;

import edu.utcn.str.lecture6.utils.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ThreadApp3 extends Thread {
    private Object lock;
    private int actMin;
    private int actMax;
    private int tranTemp;

    public ThreadApp3(Object lock, int actMin, int actMax, int tranTemp) {
        this.lock = lock;
        this.actMin = actMin;
        this.actMax = actMax;
        this.tranTemp = tranTemp;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity(1);

        synchronized (lock) {
            ActivityUtils.doTimedActivity(actMin, actMax, 2);
        }

        ActivityUtils.doActivity(3);

        // temp. transition
        try {
            Thread.sleep(tranTemp * 10);
        } catch (InterruptedException e) {
        }

        ActivityUtils.doActivity(4);
    }
}
