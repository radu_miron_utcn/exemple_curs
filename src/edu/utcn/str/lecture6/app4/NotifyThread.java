package edu.utcn.str.lecture6.app4;

import edu.utcn.str.lecture6.utils.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class NotifyThread extends Thread {
    private Object monitor1;
    private Object monitor2;
    private int actMin;
    private int actMax;
    private int tranTemp;

    public NotifyThread(Object monitor1, Object monitor2, int actMin, int actMax, int tranTemp) {
        this.monitor1 = monitor1;
        this.monitor2 = monitor2;
        this.actMin = actMin;
        this.actMax = actMax;
        this.tranTemp = tranTemp;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity(1);

        // temp. transition
        try {
            Thread.sleep(tranTemp * 10);
        } catch (InterruptedException e) {
        }

        ActivityUtils.doTimedActivity(actMin, actMax, 2);

        synchronized (monitor1){
            monitor1.notify();
        }

        synchronized (monitor2){
            monitor2.notify();
        }

        ActivityUtils.doActivity(3);
    }

}

