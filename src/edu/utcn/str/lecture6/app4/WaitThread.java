package edu.utcn.str.lecture6.app4;

import edu.utcn.str.lecture6.utils.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class WaitThread extends Thread {
    private Object monitor;
    private int actMin;
    private int actMax;

    public WaitThread(Object monitor, int actMin, int actMax) {
        this.monitor = monitor;
        this.actMin = actMin;
        this.actMax = actMax;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity(1);

        synchronized (monitor){
            try {
                monitor.wait();
            } catch (InterruptedException e) {
            }
        }

        ActivityUtils.doTimedActivity(actMin, actMax, 2);

        ActivityUtils.doActivity(3);
    }
}
