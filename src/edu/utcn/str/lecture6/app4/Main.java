package edu.utcn.str.lecture6.app4;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object monitor1 = new Object();
        Object monitor2 = new Object();

        new WaitThread(monitor1, 3, 5).start();
        new WaitThread(monitor2, 4, 6).start();
        new NotifyThread(monitor1, monitor2, 2, 3, 7).start();
    }
}
