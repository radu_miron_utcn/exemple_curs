package edu.utcn.str.lecture6.utils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ActivityUtils {

    private ActivityUtils() {
    }

    public static void doActivity(int activityNumber) {
        System.out.println("Executing activity number " + activityNumber);
    }

    public static void doTimedActivity(int min, int max, int activityNumber) {
        System.out.println("Executing activity number " + activityNumber);

        int k = (int) Math.round(Math.random() * (max - min)) + min;
        for (int i = 0; i < k * 100000; i++) {
            i++;
            i--;
        }
    }

}
